<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Posty</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">


</head>
<body class="bg-gray-200">
<nav class="p-6 bg-white flex justify-between">
    <ul class="flex items-center">

        <li>

            <a href="" class="font-Roboto p-3">Home</a>
        </li>
        <li>
            <a href="{{ route('dashboard') }}" class="font-Roboto p-3">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('posts') }}" class="font-Roboto p-3">Post</a>
        </li>
    </ul>

    <ul class="flex items-center">
        @auth
        <li>
           <a href="" class="font-Roboto p-3">{{ auth()->user()->name }}</a>
        </li>
        <li>
            <form action="{{route('logout')}}" method="post" class="inline">
                @csrf
            <button type="submit" class="font-Roboto p-3">Logout</button>
        </form>
        </li>
            @endauth
        @guest
        <li>
            <a href="{{ route('register')  }}" class="font-Roboto p-3">Register</a>
        </li>
        <li>
            <a href="{{ route('login') }}" class="font-Roboto p-3">Login</a>
        </li>
            @endguest
    </ul>

</nav>




@yield('content')

</h1>
</body>
</html>
