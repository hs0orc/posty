module.exports = {
    theme: {
        extend: {
            fontFamily: {
                'Nunito': ['Nunito', 'sans-serif'],
                'Roboto': ['Roboto'],
            }
        }
    },
}
